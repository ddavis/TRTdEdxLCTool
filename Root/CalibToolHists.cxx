#include "TRTdEdxLCTool/CalibTool.h"
#include "TRTFramework/TRTIncludes.h"
#include <vector>
#include <cmath>

EL::StatusCode dLCT::CalibTool::createOutput() {
  histoStore()->createTH1F("h_NPV",       51, -0.5, 50.5,    "; N_{PV}; Events");
  histoStore()->createTH1F("h_avgMu",     51, -0.5, 50.5,    "; #LT#mu#GT; Events");
  histoStore()->createTH1F("h_OccTrack",  66, -0.005, 0.655, "; TRT Track Occupancy; Events" );
  histoStore()->createTH1F("h_OccGlobal", 66, -0.005, 0.655, "; TRT Global Occupancy; Events" );

  std::vector<double> logBins;
  std::size_t num = 500; double from = -1.0; double to = 6.0;
  double width = (to - from) / num;
  for ( std::size_t i = 0; i < num; ++i ) logBins.push_back(std::pow(10.0,from + i*width));

  for ( const TString& type : { "" , "_truthEl" , "_truthMu" , "_truthAll" , "_contEl" , "_contMu" } ) {
    for ( const TString& reg : { "ALL" , "BRL" , "ECA" , "ECB" } ) {
      histoStore()->createTH2F    ("h2_"+reg+"_dEdx_vs_betaGamma"+type,logBins,100,0.0,3.0,";#beta#gamma;dE/dx");
      histoStore()->createTH2F    ("h2_"+reg+"_dEdx_vs_usedHits"+type, 50,0,50,100,0.0,3.0,";Used Hits;dE/dx");
      histoStore()->createTProfile("hP_"+reg+"_dEdx_vs_usedHits"+type, 50,0,50,            ";Used Hits;Mean dE/dx","s");
      histoStore()->createTProfile("hP_"+reg+"_dEdx_vs_betaGamma"+type,logBins,            ";#beta#gamma;Mean dE/dx","s");
      histoStore()->createTH2F    ("h2_"+reg+"_reso_vs_usedHits"+type, 50,0,50,200,0.0,1.0,";Used Hits;Resolution");

      histoStore()->createTH1F("h_"+reg+"_eProb_Athena"+type,        200,0.0,1.0,";;");
      histoStore()->createTH1F("h_"+reg+"_eProb_RootCore"+type,      200,0.0,1.0,";;");
      histoStore()->createTH1F("h_"+reg+"_eProb_RootCore_lowP"+type, 200,0.0,1.0,";;");
    }
  }
  histoStore()->createTH1F("h_Mass_track",400,0,2000,";;");
  histoStore()->createTH1F("h_Mass_truth",400,0,2000,";;");

  return EL::StatusCode::SUCCESS;
}
