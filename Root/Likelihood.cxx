#include "TRTdEdxLCTool/Params.h"
#include "TRTdEdxLCTool/Likelihood.h"
#include <cmath>

dLCT::Likelihood::Likelihood() {
  //Dedxcorrection::paraL_dEdx_p1   = -0.04095;
  //Dedxcorrection::paraL_dEdx_p2   = -5.06062;
  //Dedxcorrection::paraL_dEdx_p3   = -0.97276;
  //Dedxcorrection::paraL_dEdx_p4   =  0.95658;
  //Dedxcorrection::paraL_dEdx_p5   = -0.000257;
  Dedxcorrection::paraL_dEdx_p1   = -0.192682;
  Dedxcorrection::paraL_dEdx_p2   = -6.956870;
  Dedxcorrection::paraL_dEdx_p3   = -0.974757;
  Dedxcorrection::paraL_dEdx_p4   = 1.27633;
  Dedxcorrection::paraL_dEdx_p5   = -0.014986;

  Dedxcorrection::para_dEdx_p1    = -0.206938;
  Dedxcorrection::para_dEdx_p2    = -7.558950;
  Dedxcorrection::para_dEdx_p3    = -0.991186;
  Dedxcorrection::para_dEdx_p4    = 1.26495;
  Dedxcorrection::para_dEdx_p5    = -0.0043035;

  Dedxcorrection::resolution[0]   = 0.173916;
  Dedxcorrection::resolution[1]   = -0.009003;
  Dedxcorrection::resolution[2]   = 0.00028175;
  Dedxcorrection::resolution[3]   = -0.000003394;
  Dedxcorrection::resolution_e[0] = 0.1606;
  Dedxcorrection::resolution_e[1] = -0.008239;
  Dedxcorrection::resolution_e[2] = 0.0002552;
  Dedxcorrection::resolution_e[3] = -0.000002989;
}

dLCT::Likelihood::~Likelihood() {}

double dLCT::Likelihood::getTest(const double dEdx_obs,
                                 const double pTrk,
                                 dLCT::ParticleHypothesis hypothesis,
                                 dLCT::ParticleHypothesis antihypothesis,
                                 int nUsedHits,
                                 bool dividebyL) const {
  double Pone = getProb(dEdx_obs,pTrk,hypothesis,nUsedHits, dividebyL);
  double Ptwo = getProb(dEdx_obs,pTrk,antihypothesis,nUsedHits, dividebyL);
  if ( (Pone+Ptwo) != 0 ) {
    return Pone/(Pone+Ptwo);
  }
  else {
    return 0.5;
  }
}

double dLCT::Likelihood::getRes(const double dEdx_obs,
                                const double pTrk,
                                dLCT::ParticleHypothesis hypothesis,
                                bool dividebyL) const {
  double dEdx_pred  = predictdEdx(pTrk,hypothesis,dividebyL);
  double difference = dEdx_obs - dEdx_pred;
  double res        = difference/dEdx_pred;
  return std::fabs(res);
}

double dLCT::Likelihood::getProb(const double dEdx_obs,
                                 const double pTrk,
                                 dLCT::ParticleHypothesis hypothesis,
                                 int nUsedHits,
                                 bool dividebyL) const {
  double dEdx_pred = predictdEdx(pTrk, hypothesis, dividebyL); 
  if ( dEdx_pred  == 0 ) return 0.0;
  if ( hypothesis == dLCT::ParticleHypothesis::Electron ) {
    // correction for pTrk in [MeV]
    double factor = 1;
    double correct = 1+factor*(0.045*std::log10(pTrk)+0.885-1);
    dEdx_pred= dEdx_pred/correct;
  }

  double Resolution =
    Dedxcorrection::resolution[0]+
    Dedxcorrection::resolution[1]*(nUsedHits+0.5)+
    Dedxcorrection::resolution[2]*(nUsedHits+0.5)*(nUsedHits+0.5)+
    Dedxcorrection::resolution[3]*(nUsedHits+0.5)*(nUsedHits+0.5)*(nUsedHits+0.5);
  if ( hypothesis == dLCT::ParticleHypothesis::Electron ) {
    Resolution =
      Dedxcorrection::resolution_e[0]+
      Dedxcorrection::resolution_e[1]*(nUsedHits+0.5)+
      Dedxcorrection::resolution_e[2]*(nUsedHits+0.5)*(nUsedHits+0.5)+
      Dedxcorrection::resolution_e[3]*(nUsedHits+0.5)*(nUsedHits+0.5)*(nUsedHits+0.5);
  }

  double prob = std::exp( -0.5 * ( ( ( dEdx_obs - dEdx_pred ) / (Resolution*dEdx_pred) ) * 
                                   ( ( dEdx_obs - dEdx_pred ) / (Resolution*dEdx_pred) ) ));

  return prob;
}

double dLCT::Likelihood::predictdEdx(const double pTrk,
                                     dLCT::ParticleHypothesis hypothesis,
                                     bool dividebyL) const {
  double mass = 0.511;
  if ( hypothesis == dLCT::ParticleHypothesis::Pion ) {
    mass = 139.98;
  }
  double betaGamma = pTrk/mass;

  /** @todo why is it possible that a 20 MeV particle reaches this point? (see Savannah bug 94644) */
  // low momentum particle can create floating point error 
  // do we need the check in the log parameter in addition? will create CPU increase
  // do we want to throw an assertion here?
  if ( pTrk < 100 ) return 0; 
  if ( dividebyL  ) {
    if ( Dedxcorrection::paraL_dEdx_p3+1./( std::pow( betaGamma, Dedxcorrection::paraL_dEdx_p5)) <= 0 ) {
      return 0;
    }
    return Dedxcorrection::paraL_dEdx_p1 /
      std::pow( sqrt( (betaGamma*betaGamma)/(1.+(betaGamma*betaGamma)) ), Dedxcorrection::paraL_dEdx_p4)  * 
      (Dedxcorrection::paraL_dEdx_p2 -
       std::pow( sqrt( (betaGamma*betaGamma)/(1.+(betaGamma*betaGamma)) ), Dedxcorrection::paraL_dEdx_p4 ) -
       std::log(Dedxcorrection::paraL_dEdx_p3+1./( std::pow( betaGamma, Dedxcorrection::paraL_dEdx_p5) ) ) );
  }
  else {
    if ( Dedxcorrection::para_dEdx_p3+1./( std::pow( betaGamma, Dedxcorrection::para_dEdx_p5) ) <= 0 ) {
      return 0;
    }
    return Dedxcorrection::para_dEdx_p1 /
      std::pow( sqrt( (betaGamma*betaGamma)/(1.+(betaGamma*betaGamma)) ), Dedxcorrection::para_dEdx_p4)  *
      (Dedxcorrection::para_dEdx_p2 -
       std::pow( sqrt( (betaGamma*betaGamma)/(1.+(betaGamma*betaGamma)) ), Dedxcorrection::para_dEdx_p4 ) -
       std::log(Dedxcorrection::para_dEdx_p3+1./( std::pow( betaGamma, Dedxcorrection::para_dEdx_p5) ) ) );
  }
  //return 0;  
}
