#include "TRTdEdxLCTool/CalibTool.h"
#include "TRTdEdxLCTool/Likelihood.h"
#include "TRTFramework/TRTIncludes.h"
#include <memory>
#include <cstdlib>

// this is needed to distribute the algorithm to the workers
ClassImp(dLCT::CalibTool)

dLCT::CalibTool::CalibTool() : TRTAnalysis() {
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}

dLCT::CalibTool::~CalibTool() {}

EL::StatusCode dLCT::CalibTool::execute() {
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  TRTAnalysis::execute();  // Must Keep This Line

  if ( isDuplicate() || !passGRL() || hasZeroWeight() ) return EL::StatusCode::SUCCESS;

  xAOD::TEvent* event = wk()->xaodEvent();
  const xAOD::TrackParticleContainer* trackContainer = nullptr;
  if ( event->retrieve(trackContainer,"InDetTrackParticles").isFailure() ) {
    TRT::fatal("Failed to retrieve InDetTrackParticles");
  }

  histoStore()->fillTH1F("h_avgMu",  averageMu(),               weight());
  histoStore()->fillTH1F("h_NPV",    numberOfPrimaryVertices(), weight());

  for ( auto const& track : *trackContainer ) {
    if ( isGoodTrack(track) ) {
      analyzeTrack(track);
    }
    else {
      continue;
    }
  }

  auto electronContainer = lepHandler()->getElectronContainer();
  auto muonContainer     = lepHandler()->getMuonContainer();

  for ( auto electron : electronContainer ) {
    auto track = lepHandler()->getTrack(electron);
    if ( track ) {
      if ( isGoodTrack(track) ) {
        analyzeTrack(track,true,false);
      }
    }
  }
  for ( auto muon : muonContainer ) {
    auto track = lepHandler()->getTrack(muon);
    if ( track ) {
      if ( isGoodTrack(track) ) {
        analyzeTrack(track,false,true);
      }
    }
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode dLCT::CalibTool::analyzeTrack(const xAOD::TrackParticle* track, const bool contEl, const bool contMu) {
  double OT = getTrackOccupancy(track); // track occupancy
  histoStore()->fillTH1F("h_OccTrack", OT, weight());

  auto dEdxLikelihood  = std::make_shared<dLCT::Likelihood>();
  float trackMom       = track->p4().P();
  float trackMass      = track->m();
  float trackBetaGamma = trackMom/trackMass;

  float ToT_dEdx_noHT_divByL;
  float ToT_usedHits_noHT_divByL;
  float eProbAth;
  TRT::assignFromAux(track,"eProbabilityToT",         eProbAth);
  TRT::assignFromAux(track,"ToT_dEdx_noHT_divByL",    ToT_dEdx_noHT_divByL);
  TRT::assignFromAux(track,"ToT_usedHits_noHT_divByL",ToT_usedHits_noHT_divByL);
  if ( ToT_dEdx_noHT_divByL < 0.05 ) return EL::StatusCode::SUCCESS;

  auto eProbRC_noHT_divByL = dEdxLikelihood->getTest(ToT_dEdx_noHT_divByL,
                                                     trackMom,
                                                     dLCT::ParticleHypothesis::Electron,
                                                     dLCT::ParticleHypothesis::Pion,
                                                     ToT_usedHits_noHT_divByL,
                                                     true);
  auto reso_e = dEdxLikelihood->getRes(ToT_dEdx_noHT_divByL,trackMom,dLCT::ParticleHypothesis::Electron,true);
  auto reso   = dEdxLikelihood->getRes(ToT_dEdx_noHT_divByL,trackMom,dLCT::ParticleHypothesis::Pion,    true);

  auto truth = TRT::getTruthPtr(*track);
  fill2Dhists(track,truth,trackMom,trackBetaGamma,ToT_dEdx_noHT_divByL,ToT_usedHits_noHT_divByL,
              reso_e,reso,"ALL", contEl, contMu);

  filleProbHists(track, truth, eProbAth, eProbRC_noHT_divByL, "ALL", contEl, contMu);
  int etaBin5 = -999;
  if      (fabs(track->eta()) < 0.625) etaBin5 = 0;
  else if (fabs(track->eta()) < 1.070) etaBin5 = 1;
  else if (fabs(track->eta()) < 1.304) etaBin5 = 2;
  else if (fabs(track->eta()) < 1.752) etaBin5 = 3;
  else if (fabs(track->eta()) < 2.000) etaBin5 = 4;
  if ( etaBin5 == 0 ) {
    filleProbHists(track, truth, eProbAth, eProbRC_noHT_divByL, "BRL", contEl, contMu);
    fill2Dhists(track,truth,trackMom,trackBetaGamma,ToT_dEdx_noHT_divByL,ToT_usedHits_noHT_divByL,
                reso_e,reso,"BRL", contEl, contMu);
  }
  if ( etaBin5 == 2 ) {
    filleProbHists(track, truth, eProbAth, eProbRC_noHT_divByL, "ECA", contEl, contMu);
    fill2Dhists(track,truth,trackMom,trackBetaGamma,ToT_dEdx_noHT_divByL,ToT_usedHits_noHT_divByL,
                reso_e,reso,"ECA", contEl, contMu);
  }
  if ( etaBin5 == 4 ) {
    filleProbHists(track, truth, eProbAth, eProbRC_noHT_divByL, "ECB", contEl, contMu);
    fill2Dhists(track,truth,trackMom,trackBetaGamma,ToT_dEdx_noHT_divByL,ToT_usedHits_noHT_divByL,
                reso_e,reso,"ECB", contEl, contMu);
  }
  return EL::StatusCode::SUCCESS;
}

void dLCT::CalibTool::filleProbHists(const xAOD::TrackParticle* track,
                                     const xAOD::TruthParticle* truthParticle,
                                     const float eProbAth, const float eProbRC_noHT_divByL, const TString& reg,
                                     const bool contEl, const bool contMu) {
  auto w = weight();
  if ( contEl ) {
    histoStore()->fillTH1F("h_"+reg+"_eProb_Athena_contEl",  eProbAth,           w);
    histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore_contEl",eProbRC_noHT_divByL,w);
    if ( track->pt() < 10*TRT::GeV ) {
      histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore_lowP_contEl",eProbRC_noHT_divByL,w);
    }    
  }
  if ( contMu ) {
    histoStore()->fillTH1F("h_"+reg+"_eProb_Athena_contMu",  eProbAth,           w);
    histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore_contMu",eProbRC_noHT_divByL,w);
    if ( track->pt() < 10*TRT::GeV ) {
      histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore_lowP_contMu",eProbRC_noHT_divByL,w);
    }    
  }
  else {
    histoStore()->fillTH1F("h_"+reg+"_eProb_Athena",  eProbAth,           w);
    histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore",eProbRC_noHT_divByL,w);
    if ( track->pt() < 10*TRT::GeV ) {
      histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore_lowP",eProbRC_noHT_divByL,w);
    }
    if ( truthParticle != nullptr ) {
      histoStore()->fillTH1F("h_Mass_track",track->m(),w);
      histoStore()->fillTH1F("h_Mass_truth",truthParticle->m(),w);
      if ( truthParticle->isElectron() ) {
        histoStore()->fillTH1F("h_"+reg+"_eProb_Athena_truthEl",  eProbAth,           w);
        histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore_truthEl",eProbRC_noHT_divByL,w);
        if ( track->pt() < 10*TRT::GeV ) {
          histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore_lowP_truthEl",eProbRC_noHT_divByL,w);
        }
      }
      if ( truthParticle->isMuon() ) {
        histoStore()->fillTH1F("h_"+reg+"_eProb_Athena_truthMu",  eProbAth,           w);
        histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore_truthMu",eProbRC_noHT_divByL,w);
        if ( track->pt() < 10*TRT::GeV ) {
          histoStore()->fillTH1F("h_"+reg+"_eProb_RootCore_lowP_truthMu",eProbRC_noHT_divByL,w);
        }
      }
    }
  }
}

void dLCT::CalibTool::fill2Dhists(const xAOD::TrackParticle* track, const xAOD::TruthParticle* truth,
                                  const float trackP, const float betaGamma,
                                  const float ToT_dEdx_noHT_divByL, const float ToT_usedHits_noHT_divByL,
                                  const float reso_e, const float reso,
                                  const TString& reg, const bool contEl, const bool contMu) {
  auto w = weight();
  if ( contEl ) {
    histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_usedHits_contEl",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
    histoStore()->fillTH2F("h2_"+reg+"_reso_vs_usedHits_contEl",ToT_usedHits_noHT_divByL,reso_e,w);
    histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_usedHits_contEl",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
  }
  else if ( contMu ) {
    histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_usedHits_contMu",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
    histoStore()->fillTH2F("h2_"+reg+"_reso_vs_usedHits_contMu",ToT_usedHits_noHT_divByL,reso,w);
    histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_usedHits_contMu",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
  }
  else {
    histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_betaGamma",betaGamma,ToT_dEdx_noHT_divByL,w);
    histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_usedHits", ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
    histoStore()->fillTH2F("h2_"+reg+"_reso_vs_usedHits",ToT_usedHits_noHT_divByL,reso,w);
    histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_usedHits", ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
    histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_betaGamma",betaGamma,ToT_dEdx_noHT_divByL,w);
    if ( truth != nullptr ) {
      float m  = truth->m();
      float bg = trackP/m;
      histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_betaGamma_truthAll",bg,ToT_dEdx_noHT_divByL,w);
      histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_usedHits_truthAll",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
      histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_usedHits_truthAll",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
      histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_betaGamma_truthAll",bg,ToT_dEdx_noHT_divByL,w);
      if ( truth->isElectron() ) {
        float bg = trackP/0.511;
        histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_betaGamma_truthEl",bg,ToT_dEdx_noHT_divByL,w);
        histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_usedHits_truthEl",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
        histoStore()->fillTH2F("h2_"+reg+"_reso_vs_usedHits_truthEl",ToT_usedHits_noHT_divByL,reso_e,w);
        histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_usedHits_truthEl",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
        histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_betaGamma_truthEl",betaGamma,ToT_dEdx_noHT_divByL,w);
      }
      if ( truth->isMuon() ) {
        float bg = trackP/105.658;
        histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_betaGamma_truthMu",bg,ToT_dEdx_noHT_divByL,w);
        histoStore()->fillTH2F("h2_"+reg+"_dEdx_vs_usedHits_truthMu",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
        histoStore()->fillTH2F("h2_"+reg+"_reso_vs_usedHits_truthMu",ToT_usedHits_noHT_divByL,reso,w);
        histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_usedHits_truthMu",ToT_usedHits_noHT_divByL,ToT_dEdx_noHT_divByL,w);
        histoStore()->fillTProfile("hP_"+reg+"_dEdx_vs_betaGamma_truthMu",betaGamma,ToT_dEdx_noHT_divByL,w);
      }
    }
  }
}

bool dLCT::CalibTool::isGoodTrack(const xAOD::TrackParticle* track) const {
  int nIBLhits  = track->auxdata<unsigned char>("numberOfInnermostPixelLayerHits");
  int nBLayhits = track->auxdata<unsigned char>("numberOfNextToInnermostPixelLayerHits");
  int nPixhits  = track->auxdata<unsigned char>("numberOfPixelHits");
  int nSCThits  = track->auxdata<unsigned char>("numberOfSCTHits");
  int nTRThits  = track->auxdata<unsigned char>("numberOfTRTHits"); 
  if ( (nIBLhits + nBLayhits) < 1  ) return false;
  if ( (nPixhits + nSCThits)  < 4  ) return false;
  if (  nTRThits              < 10 ) return false;
  float eta = std::fabs(track->eta());
  if ( eta > 2.0 || track->pt() < 0.005*TRT::GeV ) return false;
  if ( eta > 1.37 && eta < 1.52 ) return false;
  if ( std::fabs(track->d0())  > 1.5 ) return false;
  if ( std::fabs(track->z0()*std::sin(track->theta())) > 1.5 ) return false;
  return true;
}
