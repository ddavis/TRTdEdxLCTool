#ifndef TRTdEdxLCTool_Likelihood_h
#define TRTdEdxLCTool_Likelihood_h

namespace dLCT {
  enum ParticleHypothesis { Electron , Pion };
  class Likelihood {
  private:
  public:
    Likelihood();
    virtual ~Likelihood();

    double getRes(const double dEdx_obs,
                  const double pTrk,
                  dLCT::ParticleHypothesis,
                  bool dividebyL) const;
    double getProb(const double dEdx_obs,
                   const double pTrk,
                   dLCT::ParticleHypothesis hypothesis,
                   int nUsedHits, bool dividebyL) const;
    double getTest(const double dEdx_obs,
                   const double pTrk,
                   dLCT::ParticleHypothesis hypothesis,
                   dLCT::ParticleHypothesis antihypothesis,
                   int nUsedHits,
                   bool dividebyL) const;
    double predictdEdx(const double pTrk,
                       dLCT::ParticleHypothesis hypothesis,
                       bool dividebyL) const;
  };
}

#endif
