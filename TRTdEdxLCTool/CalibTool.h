#ifndef TRTdEdxLCTool_CalibTool_h
#define TRTdEdxLCTool_CalibTool_h

#include "TRTFramework/TRTAnalysis.h"

namespace dLCT {

  class CalibTool : public TRTAnalysis {
  private:
    //TTree *m_outTree; //!
    // TH1 *myHist; //!

  public:
    // this is a standard constructor
    CalibTool();
    virtual ~CalibTool();

    // these are the functions inherited from TRTAnalysis
    virtual EL::StatusCode createOutput();
    virtual EL::StatusCode execute();

    EL::StatusCode analyzeTrack(const xAOD::TrackParticle* track, const bool contEl = false, const bool contMu = false);
    bool isGoodTrack(const xAOD::TrackParticle* track) const;
    void filleProbHists(const xAOD::TrackParticle* track,
                        const xAOD::TruthParticle* truthParticle,
                        const float eProbRC, const float eProbRC_noHTdivL, const TString& reg,
                        const bool contEl, const bool contMu);
    void fill2Dhists(const xAOD::TrackParticle* track, const xAOD::TruthParticle* truth,
                     const float trackP, const float betaGamma,
                     const float ToT_dEdx_noHT_divByL, const float ToT_usedHits_noHT_divByL,
                     const float reso_e, const float reso,
                     const TString& reg, const bool contEl, const bool contMu);

    // this is needed to distribute the algorithm to the workers
    ClassDef(CalibTool, 1);
  };

}

#endif
