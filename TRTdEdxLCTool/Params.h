#ifndef TRTdEdxLCTool_Params_h
#define TRTdEdxLCTool_Params_h

namespace Dedxcorrection {
  double paraL_dEdx_p1;
  double paraL_dEdx_p2;
  double paraL_dEdx_p3;
  double paraL_dEdx_p4;
  double paraL_dEdx_p5;

  double para_dEdx_p1;
  double para_dEdx_p2;
  double para_dEdx_p3;
  double para_dEdx_p4;
  double para_dEdx_p5;

  double resolution[4];
  double resolution_e[4];
}

#endif
